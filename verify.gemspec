# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "verify/version"

Gem::Specification.new do |s|
  s.name        = "verify"
  s.version     = Verify::VERSION
  s.authors     = ["Alan Harper", "Steve Hoeksema"]
  s.email       = ["alan@sct.com.au"]
  s.homepage    = ""
  s.summary     = %q{Verify}
  s.description = %q{Verify}

  s.rubyforge_project = "verify"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_runtime_dependency "activesupport"
  s.add_development_dependency "rake"
  s.add_development_dependency "rspec"
  s.add_runtime_dependency "rack"
  s.add_runtime_dependency "signify"

end
