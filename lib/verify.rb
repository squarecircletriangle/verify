require "uri"
require "rack/utils"
require "active_support/all"

module Verify
  class Error < StandardError; end

  def self.update_url(url, new_params)
    uri = URI.parse(url)
    query = uri.query
    params = Rack::Utils.parse_nested_query(query).symbolize_keys
    uri.path = "/" if uri.path == ""
    uri.scheme = "http" if uri.scheme.nil?
    uri.query = params.update(new_params).to_query
    uri.to_s
  rescue
    nil
  end

end

require "signify"
require "verify/base"
require "verify/contact"

# def self.request(params)
#   raise Error, "Provided params must be a Hash" unless params.kind_of?(Hash)
#   raise Error, "Provided params not set" if params.empty?
# end
#
# def self.verify(params, key, slice_slug)
#   raise Error, "Provided params must be a Hash" unless params.kind_of?(Hash)
#   raise Error, "Provided params not set" if params.empty?
#   raise Error, "Key not set" if key.blank?
#   raise Error, "Expected slice slug not set" if slice_slug.blank?
# end