module Verify
  module Contact
    module Logout
      class Request < Base
        action :generate, :receive

        column :cas_host, :name => "CAS host"
        column :request_url, :name => "Request URL"

        validates_presence_of :request_url
        validates_presence_of :cas_host, :on => :generate

        def url
          Verify.update_url("http://#{cas_host}/cas/contact/logout", params)
        end

        def params
          {
            :request_url => request_url
          }.reject { |k, v| v.nil? }
        end
      end
    end
  end
end