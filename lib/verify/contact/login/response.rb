module Verify
  module Contact
    module Login
      class Response < Base
        action :generate, :receive

        column :receive_login_url, :name => "Receive Login URL"
        column :request_url, :name => "Request URL"
        column :username
        column :message, :type => :symbol

        validates_presence_of :receive_login_url, :on => :generate
        validates_presence_of :request_url, :message

        def url
          Verify.update_url(receive_login_url, params)
        end

        def params
          {
            :request_url => request_url,
            :username => username,
            :message => message,
          }.reject { |k, v| v.nil? }
        end
      end
    end
  end
end