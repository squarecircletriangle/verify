require File.expand_path(File.dirname(__FILE__) + '/../../../spec_helper')

include Verify

describe Contact::Verification::Response do

  it "should not allow initialization" do
     Contact::Verification::Response.new.should raise_error
  end

  describe "generate" do

    before do
      @valid_generate_params = {
        :slice_slug => "mirvac-hotels",
        :secure_area_id => 42,
        :request_url => "http://www.example.com/secure",
        :receive_contact_url => "http://www.example.com/receive?cat=meow",
        :receive_login_url => "http://www.example.com/login?cat=meow",
        :contact_id => 1234,
        :authorized => true,
        :key => "deadbeef",
      }

      @valid_params = @valid_generate_params.dup
      @valid_params.delete(:key)
      @valid_params.delete(:receive_contact_url)
      @valid_params.delete(:receive_login_url)
      @valid_params[:signature] = "4e882d8be7275fceb0fe6f91874ee5db62d50b60"
    end

    describe "initialization" do

      it "should generate" do
        Contact::Verification::Response.generate(@valid_generate_params)
      end

      it "should raise an error if Slice slug is not set" do
        @valid_generate_params.delete(:slice_slug)
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Slice not set/)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should raise an error if Receive Contact URL is not set" do
        @valid_generate_params.delete(:receive_contact_url)
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Contact URL not set/)
      end

      it "should raise an error if Contact ID is not set" do
        @valid_generate_params.delete(:contact_id)
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Contact ID not set/)
      end

      it "should raise an error if Key is not set" do
        @valid_generate_params.delete(:key)
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Key not set/)
      end

      it "should recognise true authorizations" do
        @valid_generate_params[:authorized] = true
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == true

        @valid_generate_params[:authorized] = "true"
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == true

        @valid_generate_params[:authorized] = 1
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == true

        @valid_generate_params[:authorized] = "1"
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == true
      end

      it "should recognise false authorizations" do
        @valid_generate_params[:authorized] = false
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == false

        @valid_generate_params[:authorized] = "false"
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == false

        @valid_generate_params[:authorized] = 0
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == false

        @valid_generate_params[:authorized] = "0"
        response = Contact::Verification::Response.generate(@valid_generate_params)
        response.authorized.should == false
      end

      it "should deny invalid authorizations" do
        @valid_generate_params[:authorized] = "meow"
        lambda { Contact::Verification::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Authorization not valid/)
      end

    end

    describe "parameters" do

      it "should generate parameters" do
        request = Contact::Verification::Response.generate(@valid_generate_params)
        request.params.should == @valid_params
      end

    end

    describe "url" do

      it "should generate a url" do
        request = Contact::Verification::Response.generate(@valid_generate_params)
        request.url.should == "http://www.example.com/receive?authorized=true&cat=meow&contact_id=1234&request_url=http%3A%2F%2Fwww.example.com%2Fsecure&secure_area_id=42&signature=4e882d8be7275fceb0fe6f91874ee5db62d50b60&slice_slug=mirvac-hotels"
      end

    end

  end

  describe "receive" do

    before do
      @valid_generate_params = {
        :slice_slug => "mirvac-hotels",
        :secure_area_id => 42,
        :request_url => "http://www.example.com/secure",
        :contact_id => 1234,
        :authorized => true,
        :key => "deadbeef",
        :signature => "4e882d8be7275fceb0fe6f91874ee5db62d50b60",
      }
    end

    describe "initialization" do

      it "should generate" do
        Contact::Verification::Response.receive(@valid_generate_params)
      end

      it "should raise an error if Slice slug is not set" do
        @valid_generate_params.delete(:slice_slug)
        lambda { Contact::Verification::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Slice not set/)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Verification::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should raise an error if Signature is not set" do
        @valid_generate_params.delete(:signature)
        lambda { Contact::Verification::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Signature not set/)
      end

      it "should raise an error if Signature does not match" do
        @valid_generate_params[:signature] = "incorrect"
        lambda { Contact::Verification::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Signature does not match/)
      end

    end

  end

end