require File.expand_path(File.dirname(__FILE__) + '/../../../spec_helper')

include Verify

describe Contact::Login::Request do

  it "should not allow initialization" do
    Contact::Login::Request.new.should raise_error
  end

  describe "generate" do

    before do
      @valid_generate_params = {
        :cas_host => "cas.local",
        :slice_slug => "mirvac-hotels",
        :secure_area_id => 42,
        :request_url => "http://www.example.com/secure",
        :receive_contact_url => "http://www.example.com/receive?cat=meow",
        :receive_login_url => "http://www.example.com/login?cat=meow",
      }

      @valid_params = @valid_generate_params.dup
      @valid_params.delete(:cas_host)
    end

    describe "initialization" do

      it "should generate" do
        Contact::Login::Request.generate(@valid_generate_params)
      end

      it "should raise an error if cas host is not set" do
        @valid_generate_params.delete(:cas_host)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /CAS host not set/)
      end

      it "should raise an error if Slice slug is not set" do
        @valid_generate_params.delete(:slice_slug)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Slice not set/)
      end

      it "should raise an error if Secure Area ID is not set" do
        @valid_generate_params.delete(:secure_area_id)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Secure Area ID not set/)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should raise an error if Receive Contact URL is not set" do
        @valid_generate_params.delete(:receive_contact_url)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Contact URL not set/)
      end

      it "should raise an error if Receive Login URL is not set" do
        @valid_generate_params.delete(:receive_login_url)
        lambda { Contact::Login::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Login URL not set/)
      end

    end

    describe "parameters" do

      it "should generate parameters" do
        request = Contact::Login::Request.generate(@valid_generate_params)
        request.params.should == @valid_params
      end

    end

    describe "url" do

      it "should generate a url" do
        request = Contact::Login::Request.generate(@valid_generate_params)
        request.url.should == "http://cas.local/cas/contact/login?receive_contact_url=http%3A%2F%2Fwww.example.com%2Freceive%3Fcat%3Dmeow&receive_login_url=http%3A%2F%2Fwww.example.com%2Flogin%3Fcat%3Dmeow&request_url=http%3A%2F%2Fwww.example.com%2Fsecure&secure_area_id=42&slice_slug=mirvac-hotels"
      end

    end

  end

  describe "receive" do

    before do
      @valid_generate_params = {
        :slice_slug => "mirvac-hotels",
        :secure_area_id => 42,
        :request_url => "http://www.example.com/secure",
        :receive_contact_url => "http://www.example.com/receive?cat=meow",
        :receive_login_url => "http://www.example.com/login?cat=meow",
        :username => "example",
        :password => "p455w0rd"
      }
    end

    describe "initialization" do

      it "should generate" do
        Contact::Login::Request.receive(@valid_generate_params)
      end

      it "should accept a nil remember flag" do
        request = Contact::Login::Request.receive(@valid_generate_params)
        request.remember.should == nil
      end

      it "should accept a falsy remember flag" do
        @valid_generate_params[:remember] = "0"
        request = Contact::Login::Request.receive(@valid_generate_params)
        request.remember.should == false
      end

      it "should accept a truthy remember flag" do
        @valid_generate_params[:remember] = 1
        request = Contact::Login::Request.receive(@valid_generate_params)
        request.remember.should == true
      end

      it "should raise an error if Slice slug is not set" do
        @valid_generate_params.delete(:slice_slug)
        lambda { Contact::Login::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Slice not set/)
      end

      it "should raise an error if Secure Area ID is not set" do
        @valid_generate_params.delete(:secure_area_id)
        lambda { Contact::Login::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Secure Area ID not set/)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Login::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should raise an error if Receive Contact URL is not set" do
        @valid_generate_params.delete(:receive_contact_url)
        lambda { Contact::Login::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Contact URL not set/)
      end

      it "should raise an error if Receive Login URL is not set" do
        @valid_generate_params.delete(:receive_login_url)
        lambda { Contact::Login::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Login URL not set/)
      end

    end

  end

end