require File.expand_path(File.dirname(__FILE__) + '/../../../spec_helper')

include Verify

describe Contact::Login::Response do

  it "should not allow initialization" do
     Contact::Login::Response.new.should raise_error
  end

  describe "generate" do

    before do
      @valid_generate_params = {
        :request_url => "http://www.example.com/secure",
        :receive_login_url => "http://www.example.com/login?cat=meow",
        :message => :invalid,
        :username => "example",
      }

      @valid_params = @valid_generate_params.dup
      @valid_params.delete(:receive_login_url)
    end

    describe "initialization" do

      it "should generate" do
        Contact::Login::Response.generate(@valid_generate_params)
      end

      it "should allow nil username" do
        @valid_generate_params.delete(:username)
        lambda { Contact::Login::Response.generate(@valid_generate_params) }.should_not raise_error
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Login::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should raise an error if Receive Login URL is not set" do
        @valid_generate_params.delete(:receive_login_url)
        lambda { Contact::Login::Response.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Receive Login URL not set/)
      end

    end

    describe "parameters" do

      it "should generate parameters" do
        request = Contact::Login::Response.generate(@valid_generate_params)
        request.params.should == @valid_params
      end

    end

    describe "url" do

      it "should generate a url" do
        request = Contact::Login::Response.generate(@valid_generate_params)
        request.url.should == "http://www.example.com/login?cat=meow&message=invalid&request_url=http%3A%2F%2Fwww.example.com%2Fsecure&username=example"
      end

    end

  end

  describe "receive" do

    before do
      @valid_generate_params = {
        :request_url => "http://www.example.com/secure",
        :message => :invalid,
      }
    end

    describe "initialization" do

      it "should generate" do
        Contact::Login::Response.receive(@valid_generate_params)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Login::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

      it "should not raise an error if Username is not set" do
        @valid_generate_params.delete(:username)
        lambda { Contact::Login::Response.receive(@valid_generate_params) }.should_not raise_error
      end

      it "should raise an error if Message is not set" do
        @valid_generate_params.delete(:message)
        lambda { Contact::Login::Response.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Message not set/)
      end

    end

  end

end