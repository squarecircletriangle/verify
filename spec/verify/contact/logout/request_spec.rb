require File.expand_path(File.dirname(__FILE__) + '/../../../spec_helper')

include Verify

describe Contact::Logout::Request do

  it "should not allow initialization" do
    Contact::Logout::Request.new.should raise_error
  end

  describe "generate" do

    before do
      @valid_generate_params = {
        :cas_host => "cas.local",
        :request_url => "http://www.example.com/secure",
      }

      @valid_params = @valid_generate_params.dup
      @valid_params.delete(:cas_host)
    end

    describe "initialization" do

      it "should generate" do
        Contact::Logout::Request.generate(@valid_generate_params)
      end

      it "should raise an error if cas host is not set" do
        @valid_generate_params.delete(:cas_host)
        lambda { Contact::Logout::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /CAS host not set/)
      end
      
      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Logout::Request.generate(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

    end

    describe "parameters" do

      it "should generate parameters" do
        request = Contact::Logout::Request.generate(@valid_generate_params)
        request.params.should == @valid_params
      end

    end

    describe "url" do

      it "should generate a url" do
        request = Contact::Logout::Request.generate(@valid_generate_params)
        request.url.should == "http://cas.local/cas/contact/logout?request_url=http%3A%2F%2Fwww.example.com%2Fsecure"
      end

    end

  end

  describe "receive" do

    before do
      @valid_generate_params = {
        :request_url => "http://www.example.com/secure",
      }
    end

    describe "initialization" do

      it "should generate" do
        Contact::Logout::Request.receive(@valid_generate_params)
      end

      it "should raise an error if Request URL is not set" do
        @valid_generate_params.delete(:request_url)
        lambda { Contact::Logout::Request.receive(@valid_generate_params) }.should raise_error(Verify::Error, /Request URL not set/)
      end

    end

  end

end